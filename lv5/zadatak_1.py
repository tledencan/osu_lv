import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

from sklearn.linear_model import LogisticRegression

from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report

from matplotlib.colors import ListedColormap

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)


# a) Prikažite podatke za ucenje u x1−x2 ravnini matplotlib biblioteke pri ˇcemu podatke obojite
# s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
# marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
# cmap kojima je mogu´ce definirati boju svake klase.

#print(X_train)
#print(y_train)
#print(len(y_train))

fig, ax = plt.subplots()

scatter = ax.scatter(X_train[:,0], X_train[:,1], c=y_train, s=12,cmap = ListedColormap(["purple", "green"]))

# produce a legend with the unique colors from the scatter
legend1 = ax.legend(*scatter.legend_elements(),
                    loc='upper right', title="Classes")
ax.add_artist(legend1)

#test data
scatter = ax.scatter(X_test[:,0], X_test[:,1], c=y_test, s=12, cmap = ListedColormap(["black", "blue"]), marker="x")

# produce a legend with the unique colors from the scatter
legend1 = ax.legend(*scatter.legend_elements(),
                    loc='lower left', title="Classes-test")
ax.add_artist(legend1)

plt.show()

# b) Izgradite model logistiˇcke regresije pomo´cu scikit-learn biblioteke na temelju skupa podataka
# za uˇcenje.

# inicijalizacija i ucenje modela logisticke regresije
LogRegression_model = LogisticRegression()
LogRegression_model.fit( X_train , y_train )

# predikcija na skupu podataka za testiranje
y_test_p = LogRegression_model.predict( X_test )

# c) Pronadite u atributima izgradenog modela parametre modela. Prikažite granicu odluke
# nauˇcenog modela u ravnini x1 −x2 zajedno s podacima za uˇcenje. Napomena: granica
# odluke u ravnini x1−x2 definirana je kao krivulja: θ0+θ1x1+θ2x2 = 0.

print("Parametri logistice regresije")
print("theta_0: " + str(LogRegression_model.intercept_[0]))
print("theta_1: " + str(LogRegression_model.coef_[0][0]))
print("theta_2: " + str(LogRegression_model.coef_[0][1]))

xp = np.array([X_train[:,1].min(), X_train[:,1].max()])
yp1 = -LogRegression_model.coef_[0][0]/LogRegression_model.coef_[0][1] * xp[0] - LogRegression_model.intercept_[0]/LogRegression_model.coef_[0][1]
yp2 = -LogRegression_model.coef_[0][0]/LogRegression_model.coef_[0][1] * xp[1] - LogRegression_model.intercept_[0]/LogRegression_model.coef_[0][1]
yp = np.array([yp1,yp2])

colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
cmap = ListedColormap(colors[:len(np.unique(y))])
x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, 0.2),np.arange(x2_min, x2_max, 0.2))
Z = LogRegression_model.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
Z = Z.reshape(xx1.shape)
plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
                           
plt.scatter(X_train[:,0], X_train[:,1], c=y_train, s=12,cmap = cmap )#mcolors.ListedColormap(["purple", "green"])
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, s=12, cmap = cmap, marker="x")#mcolors.ListedColormap(["black", "blue"])
plt.plot(xp,yp,'r')
plt.show()

# d) Provedite klasifikaciju skupa podataka za testiranje pomoc´u izgrad¯enog modela logisticˇke
# regresije. Izraˇcunajte i prikažite matricu zabune na testnim podacima. Izraˇcunate toˇcnost,
# preciznost i odziv na skupu podataka za testiranje.

# tocnost
print (" Tocnost : " , accuracy_score(y_test , y_test_p))
# matrica zabune
cm = confusion_matrix( y_test , y_test_p )
print (" Matrica zabune : " , cm)
disp = ConfusionMatrixDisplay(cm)
disp.plot()
plt.show()
# report
print ( classification_report(y_test , y_test_p))

# e) Prikažite skup za testiranje u ravnini x1−x2. Zelenom bojom oznaˇcite dobro klasificirane
# primjere dok pogrešno klasificirane primjere oznaˇcite crnom bojom.

color=['green' if y == y_test_p[idx] else 'black' for idx, y in enumerate(y_test)]
plt.scatter(X_test[:,0], X_test[:,1], c=color)

plt.show()

# • Točnije, granica odluke je hiperravnina ΘTx=0 (za jednu ulaznu veličinu je skalar,
# za dvije ulazne veličine pravac, za tri ulazne veličine ravnina, ...)