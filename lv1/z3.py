# Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji ˇ
# sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
# potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
# vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
# (npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovaraju ˇ cu poruku

from statistics import mean

def is_float(num):
    try:
        float(num)
        return True
    except ValueError:
        return False
    

array = []

while True:
    value = input("Enter a number: ")

    if value == "Done":
        break
    else:
        if(is_float(value)):
            array.append(float(value))
        else:
            print("Not a number.")

print(len(array))
print(mean(array))
print(min(array))
print(max(array))
print(array)
