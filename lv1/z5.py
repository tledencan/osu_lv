# Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ SMSSpamCollection.txt
# [1]. Ova datoteka sadrži 5574 SMS poruka pri cemu su neke ozna ˇ cene kao ˇ spam, a neke kao ham.
# Primjer dijela datoteke:
# ham Yup next stop.
# ham Ok lar... Joking wif u oni...
# spam Did you hear about the new "Divorce Barbie"? It comes with all of Ken’s stuff!
# a) Izracunajte koliki je prosje ˇ can broj rije ˇ ci u SMS porukama koje su tipa ham, a koliko je ˇ
# prosjecan broj rije ˇ ci u porukama koje su tipa spam. ˇ
# b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?


f = open("SMSSpamCollection.txt")
lines = f.readlines()

words_ham = 0
words_spam = 0
sentences_ham = 0
sentences_spam = 0
exclamation_mark_end_spam = 0

for line in lines:
    words = line.split()
    if(words[0] == "ham"):
        words_ham+=len(words)
        sentences_ham+=1
    
    elif(words[0]=="spam"):
        words_spam+=len(words)
        sentences_spam+=1

        if words[-1]=="!":
            exclamation_mark_end_spam+=1


print(words_ham/sentences_ham)
print(words_spam/sentences_spam)
print(exclamation_mark_end_spam)
