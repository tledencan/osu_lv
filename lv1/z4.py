#  Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ song.txt.
# Potrebno je napraviti rjecnik koji kao klju ˇ ceve koristi sve razli ˇ cite rije ˇ ci koje se pojavljuju u ˇ
# datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (klju ˇ c) pojavljuje u datoteci. ˇ
# Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih.

f = open("song.txt")
lines = f.readlines()
dictionary = {}

for line in lines:
    words = line.split()
    for word in words:
        if word in dictionary:
            dictionary[f"{word}"]=dictionary[f"{word}"]+1 
        else:
            dictionary[f"{word}"] = 1

only_ones = 0

for d in dictionary.values():
    if d==1:
        only_ones+=1
        
for k in dictionary.keys():
    if dictionary[k] ==1:
        print(k)
print(only_ones)
