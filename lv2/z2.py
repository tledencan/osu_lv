import numpy as np
import matplotlib . pyplot as plt

import csv


with open("lv2/data.csv", 'r') as x:
    data_list = list(csv.reader(x, delimiter=","))

#print(np.size(data_list))

data = np.array(data_list)
#print(data.shape)

# a)
print(len(data)-1)

# b)
s, height, weight = data.T.tolist()
s = np.delete(s,0)
height = np.delete(height,0) 
weight = np.delete(weight,0) 

h = height.astype(float)
w = weight.astype(float)

plt.scatter(h,w,color = 'hotpink', s=1)
plt.show()

# c)
 
height50 = [h[i] for i in range(len(h)) if i%50==0]
weight50 = [w[i] for i in range(len(w)) if i%50==0]


plt.scatter(height50, weight50, color = '#88c999', s=1)
plt.xlabel ('Height')
plt.ylabel ('Weight')
plt.title ( 'Scatter plot')

plt.show()

# d)

print(f"Min height: {min(h)}")
print(f"Max height: {max(h)}")
print(f"Arithmetic value: {np.mean(h)}")


# e) 

s = s.astype(float)
ind = (s == 1.0) #male
mh = [h[i] for i in range(len(h)) if ind[i]]
fh = [h[i] for i in range(len(h)) if ind[i] == False]

print(f"Min men height: {min(mh)}")
print(f"Max men height: {max(mh)}")
print(f"Arithmetic value men: {np.mean(mh)}")

print(f"Min women height: {min(fh)}")
print(f"Max women height: {max(fh)}")
print(f"Arithmetic value women: {np.mean(fh)}")

# m=(data[:,0]==1)