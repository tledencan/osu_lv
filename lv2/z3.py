import numpy as np
import matplotlib . pyplot as plt

img = plt.imread ("lv2/road.jpg")
img = img [:,:,0].copy()
print ( img.shape )
print ( img.dtype )
plt.figure()
plt.imshow(img, cmap ="gray")
plt.show()

# a)
plt.imshow(img, cmap ="gray", alpha = 0.4)
plt.show()

# b)

height, width = img.shape

start_col = round(width / 4)
end_col = round(width / 2)
sliced_img = img[:, start_col:end_col]

plt.imshow(sliced_img,cmap ="gray")



# c)

rot_image = np.rot90(img,3) #-1
plt.imshow(rot_image,cmap ="gray")
plt.show()

# d)

mirrored_img = np.fliplr(img)

plt.imshow(mirrored_imgcmap ="gray")
plt.title("Mirrored Image")

plt.show()