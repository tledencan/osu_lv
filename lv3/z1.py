import pandas as pd

data = pd. read_csv ('lv3/data_C02_emission.csv')

# a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka veliˇcina? Postoje li izostale ili
# duplicirane vrijednosti? Obrišite ih ako postoje. Kategoriˇcke veliˇcine konvertirajte u tip
# category.

print(data.info())
print(data.duplicated().sum())
print(data.isnull().sum())

# # brisanje dupliciranih redova
# data . drop_duplicates ()
# # kada se obrisu pojedini redovi potrebno je resetirati indekse retka
# data = data.reset_index ( drop = True )


for col in ['Make','Model','Vehicle Class','Transmission','Fuel Type']:
    data[col] = data[col].astype('category')

print(data.info())


# b)Koja tri automobila ima najve´cu odnosno najmanju gradsku potrošnju? Ispišite u terminal:
#ime proizvod¯acˇa, model vozila i kolika je gradska potrošnja.

# data_min = data['Fuel Consumption City (L/100km)'].min()
# data_max = data['Fuel Consumption City (L/100km)'].max()
# highest_fuel_consumption = data[data['Fuel Consumption City (L/100km)']<=data_max].head(3)
# print(highest_fuel_consumption[['Make','Model','Fuel Consumption City (L/100km)']])


sorted_data_by_consumption = data.sort_values(by=['Fuel Consumption City (L/100km)'])
print(sorted_data_by_consumption.head(3)[['Make','Model','Fuel Consumption City (L/100km)']])
print(sorted_data_by_consumption.tail(3)[['Make','Model','Fuel Consumption City (L/100km)']])

# c) Koliko vozila ima veliˇcinu motora izme ¯ du 2.5 i 3.5 L? Kolika je prosjeˇcna C02 emisija
#plinova za ova vozila?

num_of_vehicles =  data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print(len(num_of_vehicles))
mean_CO2_emission = num_of_vehicles['CO2 Emissions (g/km)'].mean()
print(mean_CO2_emission)


# d) Koliko mjerenja se odnosi na vozila proizvo ¯ daˇca Audi? Kolika je prosjeˇcna emisija C02
#plinova automobila proizvod¯acˇa Audi koji imaju 4 cilindara?

audi_vehicles = data[data['Make']=='Audi']
print(len(audi_vehicles))
audi_CO2_emission = audi_vehicles[audi_vehicles['Cylinders'] == 4]['CO2 Emissions (g/km)'].mean()
print(audi_CO2_emission)

# e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjeˇcna emisija C02 plinova s obzirom na broj cilindara?

grouped = data.groupby('Cylinders')

for group,cylinder_group in grouped:
    print(f"Number of vehicles with {group} cylinders: {len(cylinder_group)}")
    print(f"Mean for {group} cylinders: {cylinder_group['CO2 Emissions (g/km)'].mean()}")
    
# f)Kolika je prosjeˇcna gradska potrošnja u sluˇcaju vozila koja koriste dizel, a kolika za vozila
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?

grouped_fuel_types = data.groupby('Fuel Type')
for group,group_data in grouped_fuel_types:
    print(group)

diesel_vehicles_mean = data[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].mean()
print(diesel_vehicles_mean)
diesel_consumption_median = (data[data['Fuel Type'] == 'D'])['Fuel Consumption City (L/100km)'].median()
print(diesel_consumption_median)

petrol_consumption_mean = (data[data['Fuel Type'] == 'X'])['Fuel Consumption City (L/100km)'].mean()
petrol_consumption_median = (data[data['Fuel Type'] == 'X'])['Fuel Consumption City (L/100km)'].median()
print(petrol_consumption_mean)
print(petrol_consumption_median) 

# g)Koje vozilo s 4 cilindra koje koristi dizelski motor ima najve´cu gradsku potrošnju goriva?

vehicles_wih_diesel_motor_4_cylinders = data[(data['Fuel Type'] == 'D')  &  (data['Cylinders'] == 4)]
max_city_consumption_D4 = vehicles_wih_diesel_motor_4_cylinders['Fuel Consumption City (L/100km)'].max()
result = vehicles_wih_diesel_motor_4_cylinders[vehicles_wih_diesel_motor_4_cylinders['Fuel Consumption City (L/100km)'] == max_city_consumption_D4]
print(result)

# h) Koliko ima vozila ima ruˇcni tip mjenjaˇca (bez obzira na broj brzina)?

print(len(data[data['Transmission'].str.contains('M') & ~(data['Transmission'].str.contains('AM'))]))


# i) Izracˇunajte korelaciju izmed¯u numericˇkih velicˇina. Komentirajte dobiveni rezultat.

# print(data['Engine Size (L)'].corr(data['Fuel Consumption City (L/100km)']))
# print(data['Engine Size (L)'].corr(data['Fuel Consumption Hwy (L/100km)']))
# print(data['Engine Size (L)'].corr(data['Fuel Consumption Comb (L/100km)']))
# print(data['Engine Size (L)'].corr(data['CO2 Emissions (g/km)']))

print(data.corr())
