import numpy as np
from tensorflow import keras
from keras import layers
from keras.datasets import cifar10
from keras.utils import to_categorical
# from tensorflow.keras import layers
# from tensorflow.keras.datasets import cifar10
# from tensorflow.keras.utils import to_categorical
from matplotlib import pyplot as plt

import ssl

ssl._create_default_https_context = ssl._create_unverified_context

# ucitaj CIFAR-10 podatkovni skup
(X_train, y_train), (X_test, y_test) = cifar10.load_data()
X_train = X_train[:25000,:]
y_train = y_train[:25000, :]
X_test = X_test[:5000, :]
y_test = y_test[:5000,:]
# print(X_train.shape[0])
# print(y_train.shape[0])
# print(X_test.shape[0])
# print(y_test.shape[0])

# prikazi 9 slika iz skupa za ucenje
plt.figure()
for i in range(9):
    plt.subplot(330 + 1 + i)
    plt.xticks([]),plt.yticks([])
    plt.imshow(X_train[i])

plt.show()


# pripremi podatke (skaliraj ih na raspon [0,1]])
X_train_n = X_train.astype('float32')/ 255.0
X_test_n = X_test.astype('float32')/ 255.0

# 1-od-K kodiranje
y_train = to_categorical(y_train, dtype ="uint8")
y_test = to_categorical(y_test, dtype ="uint8")

# CNN mreza
model = keras.Sequential()
model.add(layers.Input(shape=(32,32,3)))
model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Flatten())
model.add(layers.Dense(500, activation='relu'))
model.add (layers.Dropout (0.3))
model.add(layers.Dense(10, activation='softmax'))

model.summary()

# definiraj listu s funkcijama povratnog poziva
my_callbacks = [
    keras.callbacks.EarlyStopping(monitor = "val_loss" , patience = 5 , verbose = 1),
    keras.callbacks.TensorBoard(log_dir = 'logs/cnn_4_5',
                                update_freq = 100)
]


model.compile(optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy'])

model.fit(X_train_n,
            y_train,
            epochs = 20,
            batch_size = 100,
            callbacks = my_callbacks,
            validation_split = 0.1)


score = model.evaluate(X_test_n, y_test, verbose=0)
print(f'Tocnost na testnom skupu podataka: {100.0*score[1]:.2f}')



#Early stopping zaustavio na epohi 13,  Tocnost na testnom skupu podataka: 75.37

#Bilo je 40 epoha i batch size 64

#4. zadatak
# 1. ako se koristi jako velika ili jako mala veliˇcina serije?

#Ako se koristi batch size = 100 u svakoj epohi bude 450 iteracija (koje uzmu tih 100 podataka)
#U svakoj je epohi nešto manja vrijednost točnosti, ali je približno podjednaka do 8. epohe
# Epoch 13: early stopping
# Tocnost na testnom skupu podataka: 75.55 -vrijednost je malo veća 

# 2. ako koristite jako malu ili jako veliku vrijednost stope uˇcenja?

#premala stopa učenja može rezultirati dugim procesom treninga koji bi se mogao zaglaviti, 
# dok prevelika vrijednost može rezultirati prebrzim učenjem niza težina koji nije optimalan ili nestabilnim procesom treninga

#Manje stope učenja zahtijevaju više trening epoha s obzirom na manje promjene u težinama pri svakom ažuriranju, 
# dok veće stope učenja rezultiraju brzim promjenama i zahtijevaju manje epoha

#Stopa učenja koja je prevelika može uzrokovati prebrzu konvergaciju modela do optimalnog rješenja, dok stopa učenja koja je premala može uzrokovati zaglavljivanje procesa

#Korišten je optimizer adam koji prilagođava stopu učenja prema težinama modela.

# 3. ako izbacite odred¯ene slojeve iz mreže kako biste dobili manju mrežu?

# Izbacivanjem model.add(layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding='same'))
#              model.add(layers.MaxPooling2D(pool_size=(2, 2)))
#Epohe traju kraće 
# Epoch 11/20
# 450/450 [==============================] - 44s 99ms/step - loss: 0.3145 - accuracy: 0.8898 - val_loss: 0.8588 - val_accuracy: 0.7386
# Epoch 11: early stopping
# Tocnost na testnom skupu podataka: 73.54

# 4. ako za 50% smanjite veliˇcinu skupa za uˇcenje?

#Svaka epoha sadrži 225 iteracija, brže se odrade epohe, manja je točnost po epohama
# Epoch 13/20
# 225/225 [==============================] - 18s 78ms/step - loss: 0.2798 - accuracy: 0.9015 - val_loss: 1.0089 - val_accuracy: 0.7288
# Epoch 13: early stopping
# Tocnost na testnom skupu podataka: 71.78